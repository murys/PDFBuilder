/* GLOBAL VARIABLES */
var view, options;

var TARGET = null;

var VIEW_ELEMENTS = [];

var KEY = "";

/* Events History */
var HISTORY = [];
var back;

var current_size =
{
  width:595,
  height:842
}

var view_sizes =
[
  ["A4",595,842],
  ["A3",842,1191],
  ["A2",1191,1684],
  ["A1",1684,2384],
  ["A0",2384,3370]
];

var e_counter = 0;

/* CLASSES */


/* Text object*/

var TEXT = function()
{
  this.HTMLelement;

  this.create = function(event)
  {
    this.HTMLelement = document.createElement("textarea");
    this.HTMLelement.id = "elem"+e_counter++;
    this.HTMLelement.setAttribute("class","pdfElement");

    view.append(this.HTMLelement);

    wrap("#"+this.HTMLelement.id);

    var container = document.querySelector(".move_"+this.HTMLelement.id);
    container.style.top = (event.pageY-view_container.offsetTop-container.getBoundingClientRect().height/2)+"px";
    container.style.left = (event.pageX-view_container.getBoundingClientRect().x-container.getBoundingClientRect().width/2)+"px";

    setResizeEvent(this.HTMLelement.id);

    /*change font size event*/
    var font_size = document.createElement("input");
    font_size.className="font-size";
    container.append(font_size);
    font_size.setAttribute("value",15);
    view.querySelector("#"+this.HTMLelement.id).style.fontSize = 15+"px";

    font_size.onchange = function()
    {
      this.parentElement.querySelector("textarea").style.fontSize = font_size.value+"px";
    }

    /*change color event*/
    var color = document.createElement("input");
    color.setAttribute("type","color");
    color.className="color";
    container.append(color);

    color.onchange = function()
    {
      this.parentElement.querySelector("textarea").style.color = color.value;
    }

    VIEW_ELEMENTS.push(this);
  }
}

/*Image object*/

var IMAGE = function()
{
  this.HTMLelement;

  this.create = function(event)
  {
    this.HTMLelement = new Image();

    view.append(this.HTMLelement);
    this.HTMLelement.id = "elem"+e_counter++;
    this.HTMLelement.style.width = (current_size.width*0.8)+"px";
    this.HTMLelement.setAttribute("class","pdfElement");

    var reader = new FileReader();

    reader.onload = function()
    {
      document.querySelector("#elem"+(e_counter-1)).src = reader.result;
    }

    reader.readAsDataURL(event.target.files[0]);

    wrap("#"+this.HTMLelement.id);

    var container = document.querySelector(".move_"+this.HTMLelement.id);
    container.style.top = (event.pageY-view_container.offsetTop-container.getBoundingClientRect().height/2)+"px";
    container.style.left = (event.pageX-view_container.getBoundingClientRect().x-container.getBoundingClientRect().width/2)+"px";

    setResizeEvent(this.HTMLelement.id);

    VIEW_ELEMENTS.push(this);
  }
}

/* HISTORY log object, remembers the last past state of the view */

var history_log = function()
{
  this.content = [];

  this.create = function()
  {
    for(item of view.querySelectorAll("div"))
    {
      this.content.push(item.cloneNode(true));
    }

    HISTORY.push(this);
  }

  this.resume = function()
  {
    for(item of this.content)
    {
      view.append(item);
    }
  }
}

/* Events and page elements variables can be initialized only once the page is fully loaded */

document.onreadystatechange = function()
{
  view = document.querySelector(".view");
  view_container = document.querySelector(".view-container");
  options = document.querySelector(".gui.options");
  back = document.querySelector(".go_back");

  initializeHistory();

  if(document.readyState == "complete")
  {
    /* GUI Events */

    window.ondragover = function(e)
    {
      if(e.target!=document.querySelector(".file-loader"))
      {
        e.preventDefault();
        return;
      }
    }

    window.ondrop = function(e)
    {
      if(e.target!=document.querySelector(".file-loader"))
      {
        e.preventDefault();
        return;
      }
    }

    window.onkeydown = function(e)
    {
      KEY = e.key;
    }

    window.onkeyup = function()
    {
      KEY = "";
    }

    var loader = this.querySelector(".loader-container");

    view_container.ondragover = function()
    {
      loader.style.display="block";
      loader.style.opacity="1";
    }

    view_container.ondrop = function()
    {
      loader.style.display="none";
      loader.style.opacity="0";
    }

    document.querySelector(".file-loader").onclick = function(e)
    {
      return false;
    }

    document.querySelector(".file-loader").ondrop = function(e)
    {
      setTimeout(function()
      {
        new IMAGE().create(e);
      },1000);
    }

    /* If a menu section button is clicked then the options box will be shown */
    document.body.onclick = function(e)
    {
      if(!e.target.classList.contains("gui"))
        for(item of document.querySelectorAll(".gui.show"))
         item.setAttribute("class",item.className.replace("show",""));
    }

    for(item of document.querySelectorAll(".gui.menu .section:not(.save)"))
    {
        item.onclick = function()
      {
        var option_selected = document.querySelector("#"+getMinHTML(this.querySelector(".title")));
        if(option_selected)
        {
          if(options.className.indexOf("show")==-1)
            options.setAttribute("class",options.className+" show");

          for(item of document.querySelectorAll(".gui.options .section"))
            item.style.display="none";

          option_selected.style.display="block";
        }
      }
    }

    /* View size management */
    var sizes_dropdown = document.querySelector("#view_sizes");
    document.querySelector("#view_container li:first-child").innerHTML = view_sizes[0][0]+
    " : "+view_sizes[0][1]+" x "+view_sizes[0][2];
    var new_element;

    for(item of view_sizes)
    {
      new_element = document.createElement("li");
      new_element.className="gui";
      new_element.innerHTML=item[0]+" : "+item[1]+" x "+item[2];
      sizes_dropdown.append(new_element);
    }

    var size_buttons = document.querySelectorAll("#view_sizes li");

    for(item of size_buttons)
      item.onclick=function()
      {
        var text = this.innerHTML.split(":")[1].split(" x ");
        current_size.width = parseInt(text[0]);
        current_size.height = parseInt(text[1]);

        view_container.style.width=current_size.width+"px";
        view_container.style.height=current_size.height+"px";

        document.querySelector("#view_container li:first-child").innerHTML = this.innerHTML;
      };

      /* VIEW EVENTS IMPLEMENTATION */

      /* Text creation event*/
      view.ondblclick = function(e)
      {
        new TEXT().create(e);
      }
  }
}

/* Save PDF implementation */

function save()
{
  console.log("Saving PDF.");
  var doc = new jsPDF("p","pt");
  var elementHandler = {
    'img': function (element, renderer) {
      return true;
    }
  };

  for(i of view.querySelectorAll("div"))
  {
    var text_element = i.querySelector("textarea");

    if(text_element)
    {
      var text_color = hexToRgb(i.querySelector(".color").value);

      doc.setFontSize(parseInt(i.querySelector(".font-size").value));
      doc.setTextColor(text_color.r,text_color.g,text_color.b);
      doc.text(text_element.value,parseInt(i.style.left.replace("px","")),parseInt(i.style.top.replace("px",""))+parseInt(i.querySelector(".font-size").value),null,0,text_element.style.textAlign);
    }

      var image_element = i.querySelector("img");

      if(image_element)
      {
        doc.addImage(image_element,parseInt(i.style.left.replace("px","")),parseInt(i.style.top.replace("px","")),
        image_element.getBoundingClientRect().width, image_element.getBoundingClientRect().height);
      }
  }

  var source = window.document.getElementsByClassName("view")[0];
  doc.save("schifo.pdf");

  console.log("PDF succesfully saved.");
}

/* UTILS */

function getMinHTML(item)
{
  return item.innerHTML.toLowerCase().replace(" ","");
}

function wrap(id)
{
  var el = document.querySelector(id); /*Element we are going to wrap*/
  id = id.replace(".","").replace("#",""); /*Format given id*/

  el.style.display = "grid";
  /*Wrap the element with the container*/
  el.outerHTML = "<div class='move_"+id+"' style='top:20px;left:20px;position:absolute;width:"+el.getBoundingClientRect().width+"px;height:"+el.getBoundingClientRect().height+"px;'>"+el.outerHTML+"</div>";

  el = document.querySelector('.move_'+id); /*Select the element container just created*/

  /*  MOVE EVENT START  */
  /*Adding move icon to container*/
  var moveicon = document.createElement("i");
  moveicon.className="fas fa-arrows-alt";
  moveicon.title="Move Element";
  el.append(moveicon);

  var check = false; /*If this variable is false then the user is trying to move the object*/

    el.addEventListener('mousedown',function(pos)
  {
    check = false; /*reset the check variable*/

    /*This function does the actual work. When the mouse moves it changes the position of the container.*/
    var move = function(e)
    {
      /*The user has to be able to move the object ONLY if he is clicking on the move icon*/
      if(check || pos.target.className.indexOf("fa-arrows-alt")==-1)
      {
        /*If the user is not clicking on the icon remove the mousemove listener for performance*/
        document.removeEventListener('mousemove',move);
        return;
      }
      /*Container position adjustments*/
      elX = e.pageX-view.getBoundingClientRect().x-el.getBoundingClientRect().width+10;
      elY = e.pageY-view_container.offsetTop+15;
      el.style.left = (elX)+"px";
      el.style.top= (elY)+"px";
    }

    setTimeout(function(){
        document.addEventListener('mousemove',move);
  },50);
});

    document.addEventListener('mouseup',function()
  {
    check = true;/*If the user is no longer clicking disable move event*/
  });

  /*  MOVE EVENT END  */

  /*  DELETE EVENT START  */

  /*Adding delete icon to container*/
  var delicon = document.createElement("i");
  delicon.className="fas fa-times";
  delicon.title="Delete Element"
  el.append(delicon);

  delicon.onclick = function()
  {
    el.remove();
  }

  /*  DELETE EVENT END  */

  /*  RESIZE EVENT START  */

  var resizeicontopleft = document.createElement("i");
  resizeicontopleft.className="fas fa-angle-up top-left";
  el.append(resizeicontopleft);

  /* Resize event for top-left corner */
  resizeicontopleft.onmousedown = function(ol)
  {
    var check = true;

    var startW = el.getBoundingClientRect().width;
    var startH = el.getBoundingClientRect().height;

    var resize = function(ne)
    {
      if(check)
      {
        el.style.left = (ne.pageX-view.getBoundingClientRect().x)+"px";
        el.style.width = (startW+(ol.pageX-ne.pageX))+"px";
        if(KEY=="Shift")
        {
          var neW = startW-(ol.pageX-ne.pageX);
          el.style.height = (startH*(startW/neW))+"px";
          el.querySelector("img, textarea").style.height = (startH*(startW/neW))+"px";
        }
        else
        {
          el.style.top = (ne.pageY-view_container.offsetTop)+"px";
          el.style.height = (startH+(ol.pageY-ne.pageY))+"px";
          el.querySelector("img, textarea").style.height = (startH+(ol.pageY-ne.pageY))+"px";
        }
        el.querySelector("img, textarea").style.width = (startW+(ol.pageX-ne.pageX))+"px";
      }
      else
      {
        document.removeEventListener('mousemove',resize);
        return;
      }
    }

    document.onmousemove = function(ne)
    {
      resize(ne);
    }

    this.onmouseup = function(){check=false;}
  }

  var resizeicontopright = document.createElement("i");
  resizeicontopright.className="fas fa-angle-up top-right";
  el.append(resizeicontopright);

  /* Resize event for top-right corner */

  resizeicontopright.onmousedown = function(ol)
  {
    var check = true;

    var startW = el.getBoundingClientRect().width;
    var startH = el.getBoundingClientRect().height;

    var resize = function(ne)
    {
      if(check)
      {
        el.style.width = (startW-(ol.pageX-ne.pageX))+"px";
        if(KEY=="Shift")
        {
          var neW = startW-(ol.pageX-ne.pageX);
          el.style.height = (startH*(neW/startW))+"px";
          el.querySelector("img, textarea").style.height = (startH*(neW/startW))+"px";
        }
        else
        {
          el.querySelector("img, textarea").style.height = (startH+(ol.pageY-ne.pageY))+"px";
          el.style.height = (startH+(ol.pageY-ne.pageY))+"px";
          el.style.top = (ne.pageY-view_container.offsetTop)+"px";
        }
        el.querySelector("img, textarea").style.width = (startW-(ol.pageX-ne.pageX))+"px";
        el.style.left = (ne.pageX-view.getBoundingClientRect().x-el.style.width)+"px";
      }
      else
      {
        document.removeEventListener('mousemove',resize);
        return;
      }
    }

    document.onmousemove = function(ne)
    {
      resize(ne);
    }

    this.onmouseup = function(){check=false;}
  }

  var resizeiconbottomleft = document.createElement("i");
  resizeiconbottomleft.className="fas fa-angle-up bottom-left";
  el.append(resizeiconbottomleft);

  /* resize event for the bottom-left corner */

  resizeiconbottomleft.onmousedown = function(ol)
  {
    var check = true;

    var startW = el.getBoundingClientRect().width;
    var startH = el.getBoundingClientRect().height;

    var resize = function(ne)
    {
      if(check)
      {
        el.style.width = (startW+(ol.pageX-ne.pageX))+"px";
        el.style.height = (startH-(ol.pageY-ne.pageY))+"px";
        el.querySelector("img, textarea").style.width = (startW+(ol.pageX-ne.pageX))+"px";
        el.querySelector("img, textarea").style.height = (startH-(ol.pageY-ne.pageY))+"px";
        el.style.left = (ne.pageX-view.getBoundingClientRect().x)+"px";
        el.style.top = (ne.pageY-view_container.offsetTop-el.style.height)+"px";
      }
      else
      {
        document.removeEventListener('mousemove',resize);
        return;
      }
    }

    document.onmousemove = function(ne)
    {
      resize(ne);
    }

    this.onmouseup = function(){check=false;}
  }

  var resizeiconbottomright = document.createElement("i");
  resizeiconbottomright.className="fas fa-angle-up bottom-right";
  el.append(resizeiconbottomright);

  /* Resize event for the bottom-right corner */

  resizeiconbottomright.onmousedown = function(ol)
  {
    var check = true;

    var startW = el.getBoundingClientRect().width;
    var startH = el.getBoundingClientRect().height;

    var resize = function(ne)
    {
      if(check)
      {
        el.style.width = (startW-(ol.pageX-ne.pageX))+"px";
        el.style.height = (startH-(ol.pageY-ne.pageY))+"px";
        el.querySelector("img, textarea").style.width = (startW-(ol.pageX-ne.pageX))+"px";
        el.querySelector("img, textarea").style.height = (startH-(ol.pageY-ne.pageY))+"px";
      }
      else
      {
        document.removeEventListener('mousemove',resize);
        return;
      }
    }

    document.onmousemove = function(ne)
    {
      resize(ne);
    }

    this.onmouseup = function(){check=false;}
  }

  /*  RESIZE EVENT END  */

  return el;
}

/* returns the given element's 'id' or 'className' */

function getID(e)
{
  if(e.id)return '#'+e.id;
  if(e.className)return '.'+e.className;
  else return null;
}

/* Resize event management for pdfElements */

function setResizeEvent(id)
{
  document.querySelector("#"+id).onmousemove = function() // RESIZE EVENT MANAGEMENT: takes care that the parent container is resized as well
  {
    this.parentElement.style.width = this.getBoundingClientRect().width+"px";
    this.parentElement.style.height = this.getBoundingClientRect().height+"px";
  }
}

/* Initialize history events */

function initializeHistory()
{
  view.onclick = function(e)
  {
    TARGET = e.target;
    updateOptions();
    new history_log().create();
  }

  back.onclick = function()
  {
    view.innerHTML = "";

    if(HISTORY.length>0)HISTORY.pop().resume();
  }
}

/* Converts HEX color to RGB*/

function hexToRgb(hex)
{
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

/* change element text-align */

function changeAlign(align)
{
  document.querySelector("#text_align").parentElement.querySelector("li:first-child").innerHTML=align;
  if(TARGET.tagName.toLowerCase() != "textarea")
  {
    TARGET.querySelector("textarea").style.textAlign=align;
  }
  else
  {
    TARGET.style.textAlign=align;
  }
}

/* Updates the options sections */

function updateOptions()
{
  document.querySelector("#text_align").parentElement.querySelector("li:first-child").innerHTML=TARGET.style.textAlign;
}
